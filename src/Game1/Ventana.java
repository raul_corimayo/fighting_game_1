
package Game1;

import java.awt.*;
import javax.swing.*;

public class Ventana extends JComponent{
    private final static int ANCHO = 1000;
    private final static int ALTO = 600;
    Game1 game = new Game1(ANCHO,ALTO);
    
    public Ventana() {
        setPreferredSize(new Dimension(ANCHO, ALTO));
        this.add(game);//debe ir
        
    }
    
    public void paint(Graphics g) {
        game.mostrar(g);
    }

    private void dibuja() throws Exception {
        SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    paintImmediately(0, 0, ANCHO, ALTO);
                }
            });
    }

    public void cicloPrincipalJuego() throws Exception {
        //ManagerArchivo ma = new ManagerArchivo("src//Game1//NewClass.java");
        //ma.cleanAll();
        //ma.print("package Game1;public class NewClass {public void exe () {for(int i=0;i<10;i++){System.out.println(\"REC ES EL MEJOR\");}}}");
        //  Generador de codigo:
        //  *   Se reflejan las modificaciones en la proxima ejecucion . hasta ahora
        game.inic();
        //int count=0;//
        //double acc=0;//
        while (true) {//while
            //long t0,t;
            //t0=System.nanoTime();//
            game.loop();
            dibuja();
            //t=System.nanoTime();//
            //System.out.println(t-t0);//
            //acc=acc+t-t0;//
            //count++;//
            //System.out.println("fps= "+count/(acc/1000000000));//
        }
    }
}//end class
