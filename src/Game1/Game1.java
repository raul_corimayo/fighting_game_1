package Game1;
/**
 * Funciones obligatorias para la interfaz
 * 1.   inic(): inicializa el Juego
 * 2.   loop(): ejecuta la logica del juego, incluye la lectura de los comandos
 * 3.   mostrar(Graphics g): indica todas las salidas por pantalla(graficos,etc)
 *
 * NOTA por solo usar filtro (ejemplo: TCT), por mas que analice
 *      una imagen de 1x1 y la dibuje en un area de 1x1, ya consume
 *      muchisimo tiempo.  Cuantos mas filtros use peor sera el desempeño.
 * (!) :Pero no os preocupeis! porque Sir Cori tiene la solucion...jeje!
 *      debes crear una imagen con el filtro:
 *      Ejemplo:    //Atributos//
 *                  private Image img;
 *                  //Metodos//
 *                  public void inicializar{
 *                      img = TCT(mapa,Color.white);
 *                  }
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.IOException;
import java.io.Serializable;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JTextField;
public class Game1 extends JComponent implements Serializable {
//********************************************//
    Pantalla p;
    Player[] player;
    GestorSonido gs = new GestorSonido();
//********************************************//
    private Image char1,char2,escenario;
    private int ANCHO,ALTO,sleep;
    private Image img1TCT;
    private Image img2TCT;
//********************************************//
    Game1(int ANCHO, int ALTO) {
        this.ANCHO=ANCHO;
        this.ALTO=ALTO;
        cargarImagenes();
        JTextField txt = new JTextField();
        this.add(txt);//debe ir
        txt.addKeyListener(new java.awt.event.KeyListener() {
            public void keyTyped(KeyEvent e) {
                // nothing
            }
            public void keyPressed(KeyEvent e) {
                switch(e.getKeyCode()){
                    case KeyEvent.VK_A:   player[0].c.button1=true;   break;
                    case KeyEvent.VK_S:   player[0].c.button2=true;   break;
                    case KeyEvent.VK_D:   player[0].c.button3=true;   break;
                    case KeyEvent.VK_NUMPAD4:   player[0].c.button4=true;   break;
                    case KeyEvent.VK_NUMPAD5:   player[0].c.button5=true;   break;
                    case KeyEvent.VK_NUMPAD6:   player[0].c.button6=true;   break;
                    case KeyEvent.VK_UP:        player[0].c.up=true;        break;
                    case KeyEvent.VK_DOWN:      player[0].c.down=true;      break;
                    case KeyEvent.VK_LEFT:      player[0].c.left=true;      break;
                    case KeyEvent.VK_RIGHT:     player[0].c.right=true;     break;
                }
            }
            public void keyReleased(KeyEvent e) {
                switch(e.getKeyCode()){
                    case KeyEvent.VK_A:   player[0].c.button1=false;  break;
                    case KeyEvent.VK_S:   player[0].c.button2=false;  break;
                    case KeyEvent.VK_D:   player[0].c.button3=false;  break;
                    case KeyEvent.VK_NUMPAD4:   player[0].c.button4=false;  break;
                    case KeyEvent.VK_NUMPAD5:   player[0].c.button5=false;  break;
                    case KeyEvent.VK_NUMPAD6:   player[0].c.button6=false;  break;
                    case KeyEvent.VK_UP:        player[0].c.up=false;       break;
                    case KeyEvent.VK_DOWN:      player[0].c.down=false;     break;
                    case KeyEvent.VK_LEFT:      player[0].c.left=false;     break;
                    case KeyEvent.VK_RIGHT:     player[0].c.right=false;    break;
                }
            }
        });
    }
    public void cargarImagenes() {
    	String carpeta = "imagenes/";
        char1 = cargar(carpeta+"ninja.GIF");
        char2 = cargar(carpeta+"ninja.GIF");
        escenario = cargar(carpeta+"escenario.GIF");
    }

    public Image cargar(String str){
        ImageIcon img = new ImageIcon(getClass().getResource(str));
        return img.getImage();
    }
    
    public void inic() throws IOException, ClassNotFoundException {//OBLIGATORIO
        player = new Player[2];
        player[0] = new Player();
        player[1] = new Player();
        p = new Pantalla();
        player[0].init(1);
        player[1].init(2);
        p.init();
        sleep=57*2/3;
        img1TCT = TCT(char1,Color.white);
        img2TCT = TCT(TCT2(char2,Color.blue,Color.red),Color.white);
        gs.playInternalBackGround("sonido/fondo.wav");
        //gs.playFondo(gs.FONDO);


//        ObjectOutputStream objout = new ObjectOutputStream( new FileOutputStream("archivo.x"));
//        int[][] s = {{45,45},{222,10}};
//        objout.writeObject(s);
//        int[][] ss = {{0,0},{45,650}};
//        objout.writeObject(ss);
//        ObjectInputStream objin = new ObjectInputStream( new FileInputStream("archivo.x"));
//        int[][] s2 = (int[][]) objin.readObject();
//        System.out.println(s2[0][0]+""+s2[0][1]+""+s2[1][1]);
//        s2 = (int[][]) objin.readObject();
//        System.out.println(s2[0][0]+""+s2[0][1]+""+s2[1][1]);


    }

    public void loop() throws InterruptedException{//OBLIGATORIO
        Thread.sleep(sleep);
        logica();//OBLIGATORIO
    }
    //private Image TCT(BufferedImage image,Color c) {
//    private Image TCT(Image image,Color c) {
//        final int r1 = c.getRed();
//        final int g1 = c.getGreen();
//        final int b1 = c.getBlue();
//        ImageFilter filter = new RGBImageFilter() {
//            public final int filterRGB(int x, int y, int rgb) {
//                int r = (rgb & 0xFF0000) >> 16;
//                int g = (rgb & 0xFF00) >> 8;
//                int b = rgb & 0xFF;
//                if (r==r1 && g==g1 && b==b1) {
//                    return rgb & 0xFFFFFF;
//                }
//                return rgb;
//                //return rgb & 0x888888FF;//transparente 50%
//            }
//        };
//        ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
//        return Toolkit.getDefaultToolkit().createImage(ip);
//    }

    private Image TCT(Image image,final Color c) {
        //Hace exactamente lo mismo que la otra TCT
        //solo que lo hace diferente -Mas eficiente-
        ImageFilter filter = new RGBImageFilter() {
            public final int filterRGB(int x, int y, int rgb) {
                if(rgb==c.getRGB()) {
                    return rgb & 0xFFFFFF;
                }
                return rgb;
                //return rgb & 0x888888FF;//transparente 50%
            }
        };
        ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }
    //
    private Image TCT2(Image image,Color c1,final Color c2) {
        final int w = Color.white.getRed();
        ImageFilter filter = new RGBImageFilter() {

            public final int filterRGB(int x, int y, int rgb) {
                int r = (rgb & 0xFF0000) >> 16;
                int g = (rgb & 0xFF00) >> 8;
                int b = rgb & 0xFF;
                if (r==w && g==w && b==w) {
                    return rgb & 0xFFFFFF;
                }
                Color cc= new Color(r*5/10,g*5/10,b*5/10);
                rgb = cc.getRGB();
                return rgb;
            }
        };
        ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }
    int temblorMagnitud=0,temblorTiempo=0;
    public void mostrar(Graphics g){//OBLIGATORIO
        

        temblorMagnitud=-temblorMagnitud;
        g.drawImage(escenario,0,0-temblorMagnitud,ANCHO,ALTO-temblorMagnitud+1,null);

        int[] h;

        h = player[0].cargarParametrosParaDibujar();
        g.drawImage(img1TCT,h[0],h[1]-temblorMagnitud,h[2],h[3]-temblorMagnitud,h[4],h[5],h[6],h[7],null);

        h = player[1].cargarParametrosParaDibujar();
        g.drawImage(img2TCT,h[0],h[1]-temblorMagnitud,h[2],h[3]-temblorMagnitud,h[4],h[5],h[6],h[7],null);
        
        g.setColor(new Color(0, 255, 0, 150));        
        g.fillRect(40+400-4*player[0].energia, 40, 4*player[0].energia, 40);
        g.fillRect(ANCHO-40-400, 40, 4*player[1].energia, 40);
        g.setColor(new Color(255, 0, 0, 150));
        g.fillRect(40, 40, 400-4*player[0].energia, 40);
        g.fillRect(ANCHO-40-400+4*player[1].energia, 40, 400-4*player[1].energia, 40);
        g.setColor(new Color(200, 200, 200, 225));
        g.drawRoundRect(40, 40, 400, 40,10,10);
        g.drawRoundRect(ANCHO-40-400, 40, 400, 40,10,10);
        g.drawRoundRect(39, 39, 402, 42,10,10);
        g.drawRoundRect(ANCHO-41-400, 39, 402, 42,10,10);
        /*
        if(1==61){//mostrar los rectangulos de: ataq, vuln1, vuln2.//
            int [] q = player[0].qAtaque();
            // player 1 //
            g.setColor(Color.red);
            g.drawRect(q[0],q[1],q[2]-q[0],q[3]-q[1]);
            q = player[0].qVuln1();
            g.setColor(Color.green);
            g.drawRect(q[0],q[1],q[2]-q[0],q[3]-q[1]);
            q = player[0].qVuln2();
            g.drawRect(q[0],q[1],q[2]-q[0],q[3]-q[1]);
            q = player[0].qRect(0);
            // player 2 //
            q = player[1].qAtaque();
            g.setColor(Color.red);
            g.drawRect(q[0],q[1],q[2]-q[0],q[3]-q[1]);
            q = player[1].qVuln1();
            g.setColor(Color.green);
            g.drawRect(q[0],q[1],q[2]-q[0],q[3]-q[1]);
            q = player[1].qVuln2();
            g.drawRect(q[0],q[1],q[2]-q[0],q[3]-q[1]);
        }
        */
    }
    public void logica() throws InterruptedException{//OBLIGATORIO
        //selecciona pantalla
        switch(p.np){
            case Round: logicaRound();break;
            case Presentacion:break;
        }
    }
    public void logicaRound() throws InterruptedException{
    //1.detectar HITS y cambiar estados
        boolean[] hit = detectarHit();
        if(hit[0]){//golpeado P2 (Unica sec.)
        	if(0<player[1].energia)player[1].energia -= 10;
        	gs.playInternal("sonido/hit.wav");
            detectar_darseLaVuelta1();
            modificarEstadosPlayers(0);
        }
        if(hit[1]){//golpeado P1 (Unica sec.)
        	if(0<player[0].energia)player[0].energia -= 10;
        	gs.playInternal("sonido/hit.wav");
            detectar_darseLaVuelta2();
            modificarEstadosPlayers(1);
        }
    //2.darse la vuelta
        boolean[] dv = detectar_darseLaVuelta1();
        //
        //... logica ...
        //
    //3.GESTIONAR player (incluye mandos y comandos)
        player[0].gestionarPlayer();
        IAEnemy();
        player[1].gestionarPlayer();
        if(0<temblorTiempo){
            temblorTiempo--;
        }else{
            temblorMagnitud=0;
        }
    }
    public void modificarEstadosPlayers(int id){
    //modifica el estado de los players, el player (id) golpeo al otro player
        int idEnemy = 1-id;
        player[id].ofensivo=false;
        if(player[idEnemy].s.nombre==NameEstado.SaltoAd
                || player[idEnemy].s.nombre==NameEstado.SaltoAt
                || player[idEnemy].s.nombre==NameEstado.SaltoN
                || player[idEnemy].s.nombre==NameEstado.GolpeAireGD
                || player[idEnemy].s.nombre==NameEstado.GolpeAireGM
                || player[idEnemy].s.nombre==NameEstado.GolpeAireGF
                || player[idEnemy].s.nombre==NameEstado.GolpeadoAire1)
        {
            player[idEnemy].vy=-30;
            player[idEnemy].vx=-player[idEnemy].sg();
            player[idEnemy].s.GolpeadoAire1();
        }else{
            player[idEnemy].s.GolpeadoN1();
        }
        player[idEnemy].t=0;
        player[id].suspenderHit=3;
        player[idEnemy].temblarHit=3;
        temblorMagnitud=1;
        temblorTiempo=3;
    }
    public void IAEnemy(){
        if(player[0].px+400<player[1].px){
            player[1].c.up=true;
            player[1].c.left=true;
            player[1].c.button1=false;
            player[1].c.right=false;
        }else if(player[1].px+400<player[0].px){
            player[1].c.up=true;
            player[1].c.left=false;
            player[1].c.button1=false;
            player[1].c.right=true;
        }else if((player[0].px-player[1].px)<100){
            player[1].c.up=false;
            player[1].c.left=false;
            player[1].c.button1=true;
            player[1].c.right=false;
        }else{
            player[1].c.up=false;
            player[1].c.left=false;
            player[1].c.button1=false;
            player[1].c.right=true;
        }
        if(player[0].c.button1 || player[0].c.button2 || player[0].c.button3){
            player[1].c.up=false;
            player[1].c.left=false;
            player[1].c.button1=true;
            player[1].c.right=false;
        }
        if(player[0].c.up){
            player[1].c.up=true;
            player[1].c.left=false;
            player[1].c.button1=false;
            player[1].c.right=false;
        }
    }
    public boolean[] detectarHit(){
        boolean cond1=false, cond2=false, cond3=false, cond4=false;
        if(player[0].ofensivo && player[1].vulnerable){
            cond1 = intersec(player[0].qAtaque(),player[1].qVuln1());
            cond2 = intersec(player[0].qAtaque(),player[1].qVuln2());
        }
        if(player[1].ofensivo && player[0].vulnerable){
            cond3 = intersec(player[1].qAtaque(),player[0].qVuln1());
            cond4 = intersec(player[1].qAtaque(),player[0].qVuln2());
        }
        boolean[] aux = {false,false};
        if (cond1 || cond2){aux[0]=true; /*System.out.writeln("player[0] ataco a player[1]");*/}
        if (cond3 || cond4){aux[1]=true; /*System.out.writeln("player[1] ataco a player[0]");*/}
        return aux;
    }
    public boolean intersec(int[] a, int[] b){
        //{ [0]=x0 , [1]=y0 , [2]=xf , [3]=yf }
        int ax0,ay0,axf,ayf, vx0,vy0,vxf,vyf ;
        ax0=a[0]; ay0=a[1]; axf=a[2]; ayf=a[3];//a
        vx0=b[0]; vy0=b[1]; vxf=b[2]; vyf=b[3];//b
        boolean interX,interY;
        interX = ( (ax0<vx0 && vx0<axf)||(ax0<vxf && vxf<axf)||(vx0<ax0 && ax0<vxf) );
        interY = ( (ay0<vy0 && vy0<ayf)||(ay0<vyf && vyf<ayf)||(vy0<ay0 && ay0<vyf) );
        return (interX && interY);
    }
    public boolean[] detectar_darseLaVuelta1(){
        boolean[] aux = {false, false};
        //player[0].s.nombre == NameEstado.Normal &&
        if((player[0].s.nombre == NameEstado.Normal) && 0<player[0].px-player[1].px){player[0].lado=2;}
        if((player[1].s.nombre == NameEstado.Normal) && 0<player[0].px-player[1].px){player[1].lado=1;}
        if((player[0].s.nombre == NameEstado.Normal) && player[0].px-player[1].px<0){player[0].lado=1;}
        if((player[1].s.nombre == NameEstado.Normal) && player[0].px-player[1].px<0){player[1].lado=2;}
        return aux;
    }
    public boolean[] detectar_darseLaVuelta2(){
        boolean[] aux = {false, false};
        //player[0].s.nombre == NameEstado.Normal &&
        if((player[0].s.nombre == NameEstado.GolpeadoAire1 || player[0].s.nombre == NameEstado.GolpeadoN1) && 0<player[0].px-player[1].px){player[0].lado=2;}
        if((player[1].s.nombre == NameEstado.GolpeadoAire1 || player[1].s.nombre == NameEstado.GolpeadoN1) && 0<player[0].px-player[1].px){player[1].lado=1;}
        if((player[0].s.nombre == NameEstado.GolpeadoAire1 || player[0].s.nombre == NameEstado.GolpeadoN1) && player[0].px-player[1].px<0){player[0].lado=1;}
        if((player[1].s.nombre == NameEstado.GolpeadoAire1 || player[1].s.nombre == NameEstado.GolpeadoN1) && player[0].px-player[1].px<0){player[1].lado=2;}
        return aux;
    }



}//end class
