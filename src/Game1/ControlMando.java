
package Game1;

public class ControlMando {

//ATRIBUTOS-------------------------------------------------------------------//
//Mandos
    public boolean up = false;
    public boolean down = false;
    public boolean left = false;
    public boolean right = false;
//Comandos
    public boolean button1 = false;//GD
    public boolean button2 = false;//GM
    public boolean button3 = false;//GF
    public boolean button4 = false;//PD
    public boolean button5 = false;//PM
    public boolean button6 = false;//PF
//METODOS---------------------------------------------------------------------//
    public void releaseAll(){//Pone en false todas las banderas
        up    = false;
        down  = false;
        left  = false;
        right = false;
        button1 = false;//GD
        button2 = false;//GM
        button3 = false;//GF
        button4 = false;//PD
        button5 = false;//PM
        button6 = false;//PF
    }

}//end class

