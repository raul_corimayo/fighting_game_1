package Game1;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class GestorSonido {
	

    /**
     * USO: en la clase que los llame usar constantes de tipo String con la url
     */
    
    private static Clip backGround;
    
//Metodos---------------------------------------------------------------------//
    
    /*
     * EFICIENCIA MULTIPANTALLA 
     * 1.	podria cargar todas las rutas de todos los sonidos de las pantallas
     * 2.	podria tener constantes (byte) para los sonidos, para poder usar
     * 3.	podrian ser estaticos los metodos parapoder llamarlos de cualquir clase
     * 		sin necesidad de instanciar para cada clase que lo utilice 
     * 		el metodo playde la forma antigua:  gs.play(gs.TRUENO);
     * 4.	despues de todo solo almacenaria las rutas y no los sonidos en si
     * +	puede ser llamado desde cualquier clase
     * -	gs dependiente de cada aplicacion ,abrir la clase para adaptarlo
     * */
    
    
    public void playExternal(String url) {
        //pueden ser sonidos locales (que pertenecen al framework (propios de el))
        //pueden ser tambien, por supuesto, sonidos externos
        //url = src/gameFrameWorkRec/d/exp.wav
    	try{
            Hilo h=new Hilo("ThreadSound "+url,url,false);
            h.start();
    	} catch(Exception e){
            System.out.println("Can't play sound: Index Overflow");
    	}
    }
    
    public void playInternal(String url) {
        //pueden ser sonidos locales (que pertenecen al framework (propios de el))
        //pueden ser tambien, por supuesto, sonidos externos
        //url = exp.wav
    	try{
            Hilo h=new Hilo("ThreadSound "+url,url,true);
            h.start();
    	} catch(Exception e){
            System.out.println("Can't play sound: Index Overflow");
    	}
    }
    
    public void playExternalBackGround(String url){
        try{
            AudioInputStream audioStream;
            File file = new File(url);
            audioStream =  AudioSystem.getAudioInputStream(file);       
            backGround = AudioSystem.getClip();
            backGround.open(audioStream);
            backGround.loop(Clip.LOOP_CONTINUOUSLY);
            backGround.start();
            
        }catch(Exception e){
            System.out.println("Can't play music");
        }
    }
    
    public void playInternalBackGround(String url){
        InputStream path = getClass().getResourceAsStream(url);
        try{
            AudioInputStream audioStream;
            InputStream bufferedIn = new BufferedInputStream(path);
            audioStream =  AudioSystem.getAudioInputStream(bufferedIn);
            backGround = AudioSystem.getClip();
            backGround.open(audioStream);
            backGround.loop(Clip.LOOP_CONTINUOUSLY);
            backGround.start();
            
            
        }catch(Exception e){
            System.out.println("Can't play music");
        }
    }
    
    public void stopBackGround(){
        //sirve para detener sonidos de fonde ya sean INTERNOS o EXTERNOS 
        backGround.stop();
        backGround.close();
        backGround = null;
    }
	
}//end class
