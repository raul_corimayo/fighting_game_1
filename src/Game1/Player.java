package Game1;
//

import java.io.IOException;


public class Player{
//Atributos-------------------------------------------------------------------//

    ControlMando c;
    Estado s;
    int t,px,py,vx,vy,lado;
    boolean ofensivo,vulnerable;
    int temblarHit,suspenderHit;//cuando ocurre un hit: un player suspende, el otro tiembla.
    int energia = 100;
    
//Metodos---------------------------------------------------------------------//
    public void init (int id) throws IOException, ClassNotFoundException {
        
        s = new Estado();
        c = new ControlMando();
        s.init(id);
        t=0;
        if(id==1){px=200;lado=1;}
        else     {px=600;lado=2;}
        py=430;
        vx=0;
        vy=0;
        ofensivo=false;
        vulnerable=true;
        temblarHit=0;
        suspenderHit=0;
    }
    public int[] cargarParametrosParaDibujar(){
        int b0,a0,bf,af,x0,y0,xf,yf;
        if(lado==1){
            b0 = px - 2*s.d1x(t);
            a0 = py - 2*s.d1y(t);
            bf = 2*s.d2x(t) + px;
            af = 2*s.d2y(t) + py;
            x0 = s.x0(t);
            y0 = s.y0(t);
            xf = s.x(t);
            yf = s.y(t);
        }else{
            b0 = px - 2*s.d2x(t);//
            a0 = py - 2*s.d1y(t);
            bf = 2*s.d1x(t) + px;//
            af = 2*s.d2y(t) + py;
            x0 = s.x(t);//
            y0 = s.y0(t);
            xf = s.x0(t);//
            yf = s.y(t);
        }
        int[] aux = {b0,a0,bf,af,x0,y0,xf,yf};
        return aux;
    }
    public int[] qAtaque(){
        return qRect(3);
    }
    public int[] qVuln1(){
        return qRect(1);
    }
    public int[] qVuln2(){
        return qRect(2);
    }
    public int[] qRect(int tipo){
        int qx0,qy0,qxf,qyf;
        if(lado==1){
            qx0=px+2*s.dqx0(t,tipo);
            qxf=px+2*s.dqxf(t,tipo);
        }else{
            qx0=px-2*s.dqxf(t,tipo);
            qxf=px-2*s.dqx0(t,tipo);
        }
        qyf=py+2*s.dqyf(t,tipo);
        qy0=py+2*s.dqy0(t,tipo);
        int[] aux={qx0,qy0,qxf,qyf};
        return aux;
    }
    public void gestionarPlayer(){
        if(temblarHit>0){suspenderHit=0;temblarHit();}
        else if(suspenderHit>0){suspenderHit();}
        else{
            t++;
            switch(s.nombre){
                case Normal: estadoNormal();break;
                case NormalAvanza: estadoNormalAvanza();break;
                case Avanza: estadoAvanza();break;
                case NormalRetrocede: estadoNormalRetrocede();break;
                case Retrocede: estadoRetrocede();break;
                case GolpeNGD: estadoGolpeNGD();break;
                case GolpeNGM: estadoGolpeNGM();break;
                case GolpeNGF: estadoGolpeNGF();break;
                case GolpeadoN1: estadoGolpeadoN1();break;
                case GolpeadoAire1: estadoGolpeadoAire1();break;
                case SaltoAd: estadoSaltoAd();break;
                case SaltoAt: estadoSaltoAt();break;
                case SaltoN: estadoSaltoN();break;
                case GolpeAireGD: estadoGolpeAireGD();break;
                case GolpeAireGM: estadoGolpeAireGM();break;
                case GolpeAireGF: estadoGolpeAireGF();break;
            }
        }
        
    }





    public void estadoNormal(){
        if(c.button1){ofensivo=true;s.GolpeNGD();t=0;}
        else if(c.button2){ofensivo=true;s.GolpeNGM();t=0;}
        else if(c.button3){ofensivo=true;s.GolpeNGF();t=0;}
        else if(avanza()&& c.up){s.SaltoAd();vy=-50;t=0;}
        else if(retrocede()&& c.up){s.SaltoAt();vy=-50;t=0;}
        else if(c.up){s.SaltoN();vy=-50;t=0;}
        else if(avanza()){s.NormalAvanza();t=0;}
        else if(retrocede()){s.NormalRetrocede();t=0;}
        else{if(t>=s.n){t=0;}}
    }
    public void estadoNormalAvanza(){
        px=px+sg()*15;
        if(c.button1){ofensivo=true;s.GolpeNGD();t=0;}
        else if(c.button2){ofensivo=true;s.GolpeNGM();t=0;}
        else if(c.button3){ofensivo=true;s.GolpeNGF();t=0;}
        else if(avanza()&& c.up){s.SaltoAd();vy=-50;t=0;}
        else if(retrocede()&& c.up){s.SaltoAt();vy=-50;t=0;}
        else if(c.up){s.SaltoN();vy=-50;t=0;}
        else if(avanza()){
            if(t>=s.n){
                s.Avanza();t=0;
            }else{
                s.NormalAvanza();
            }
        }else{
            s.Normal();t=0;
        }
    }
    public void estadoAvanza(){
        px=px+sg()*15;
        if(c.button1){ofensivo=true;s.GolpeNGD();t=0;}
        else if(c.button2){ofensivo=true;s.GolpeNGM();t=0;}
        else if(c.button3){ofensivo=true;s.GolpeNGF();t=0;}
        else if(avanza()&& c.up){s.SaltoAd();vy=-50;t=0;}
        else if(retrocede()&& c.up){s.SaltoAt();vy=-50;t=0;}
        else if(c.up){s.SaltoN();vy=-50;t=0;}
        else if(avanza()){if(t>=s.n){t=0;}}
        else{s.Normal();t=0;}
    }
    public void estadoRetrocede(){
        px=px-sg()*15;
        if(c.button1){ofensivo=true;s.GolpeNGD();t=0;}
        else if(c.button2){ofensivo=true;s.GolpeNGM();t=0;}
        else if(c.button3){ofensivo=true;s.GolpeNGF();t=0;}
        else if(avanza()&& c.up){s.SaltoAd();vy=-50;t=0;}
        else if(retrocede()&& c.up){s.SaltoAt();vy=-50;t=0;}
        else if(c.up){s.SaltoN();vy=-50;t=0;}
        else if(retrocede()){if(t>=s.n){t=0;}}
        else{s.Normal();t=0;}
    }
    public void estadoNormalRetrocede(){
        px=px-sg()*15;
        if(c.button1){ofensivo=true;s.GolpeNGD();t=0;}
        else if(c.button2){ofensivo=true;s.GolpeNGM();t=0;}
        else if(c.button3){ofensivo=true;s.GolpeNGF();t=0;}
        else if(avanza()&& c.up){s.SaltoAd();vy=-50;t=0;}
        else if(retrocede()&& c.up){s.SaltoAt();vy=-50;t=0;}
        else if(c.up){s.SaltoN();vy=-50;t=0;}
        else if(retrocede()){
            if(t>=s.n){
                s.Retrocede();t=0;
            }else{
                s.NormalRetrocede();
            }
        }else{
            s.Normal();t=0;
        }
    }
    public void estadoSaltoAd(){
        px=px+sg()*20;
        py=py+vy;
        vy=vy+8;
        vx=+sg()*20;
        if(c.button1){ofensivo=true;s.GolpeAireGD();t=0;}
        else if(c.button2){ofensivo=true;s.GolpeAireGM();t=0;}
        else if(c.button3){ofensivo=true;s.GolpeAireGF();t=0;}
        if(t>=s.n){t=t-1;}
        if(py>=430){py=430;vy=0;s.Normal();t=0;}
    }
    public void estadoSaltoAt(){
        px=px-sg()*20;
        py=py+vy;
        vy=vy+8;
        vx=-sg()*20;
        if(c.button1){ofensivo=true;s.GolpeAireGD();t=0;}
        else if(c.button2){ofensivo=true;s.GolpeAireGM();t=0;}
        else if(c.button3){ofensivo=true;s.GolpeAireGF();t=0;}
        if(t>=s.n){t=t-1;}
        if(py>=430){py=430;vy=0;s.Normal();t=0;}
    }
    public void estadoSaltoN(){
        py=py+vy;
        vy=vy+8;
        vx=0;
        if(c.button1){ofensivo=true;s.GolpeAireGD();t=0;}
        else if(c.button2){ofensivo=true;s.GolpeAireGM();t=0;}
        else if(c.button3){ofensivo=true;s.GolpeAireGF();t=0;}
        if(t>=s.n){t=t-1;}
        if(py>=430){py=430;vy=0;s.Normal();t=0;}
    }
    public void estadoGolpeAireGD(){
        py=py+vy;
        vy=vy+8;
        px=px+vx;
        c.button1=false;
        if(t>=s.n){//cuando termina el tiempo del estado
            if(vx==0){
                s.SaltoN();
            }else{
                if(vx<0){
                    s.SaltoAt();
                }else{
                    s.SaltoAd();
                }
            }//nota NO debe ir t=0;
        }
        if(py>=430){py=430;vy=0;s.Normal();t=0;}
    }
    public void estadoGolpeAireGM(){
        py=py+vy;
        vy=vy+8;
        px=px+vx;
        c.button1=false;
        if(t>=s.n){//cuando termina el tiempo del estado
            if(vx==0){
                s.SaltoN();
            }else{
                if(vx<0){
                    s.SaltoAt();
                }else{
                    s.SaltoAd();
                }
            }//nota NO debe ir t=0;
        }
        if(py>=430){py=430;vy=0;s.Normal();t=0;}
    }
    public void estadoGolpeAireGF(){
        py=py+vy;
        vy=vy+8;
        px=px+vx;
        c.button1=false;
        if(t>=s.n){//cuando termina el tiempo del estado
            if(vx==0){
                s.SaltoN();
            }else{
                if(vx<0){
                    s.SaltoAt();
                }else{
                    s.SaltoAd();
                }
            }//nota NO debe ir t=0;
        }
        if(py>=430){py=430;vy=0;s.Normal();t=0;}
    }
    //Futura Evolucion : los estados de los golpes tienen mucho en comun...
    //       se ueden agrupar en superestados que los gestionen a todos
    //< Idem para otros estados >
    //       c.buttones(false);
    //       if(t>=s.n){s.Normal();t=0;}
    public void estadoGolpeNGD(){
        c.button1=false;
        if(t>=s.n){s.Normal();t=0;}
    }
    public void estadoGolpeNGM(){
        c.button2=false;
        if(t>=s.n){s.Normal();t=0;}
    }
    public void estadoGolpeNGF(){
        c.button3=false;
        if(t<=s.n){px=px+2*(s.n-t)*sg();}
        if(t>=s.n){s.Normal();t=0;}
    }
    public void estadoGolpeadoN1(){
        if(t<4)px=px-sg()*2*(4-t)*(4-t);
        if(t>=s.n){s.Normal();t=0;}
    }
    public void estadoGolpeadoAire1(){
        if(t>=s.n){s.SaltoN();t=0;}// no va t=0;
        py=py+vy;
        vy=vy+4;
        px=px-sg();
        if(py>=430){py=430;vy=0;s.Normal();t=0;}// = si toca el suelo
        //    !--->esto se debe colocar en funcion de  <  s.d2y(t)  >
    }
//-----------Funciones auxiliares---------------------------------------------//
    public int sg(){
        if(lado==1){
            return +1;
        }else{
            return -1;
        }
    }
    public boolean avanza(){
        if(lado==1){
            return c.right;
        }else{
            return c.left;
        }
    }
    public boolean retrocede(){
        if(lado==1){
            return c.left;
        }else{
            return c.right;
        }
    }
//-----Funciones Utiles Para La SuperClase (Interface)------------------------//
    public void temblarHit(){
        switch (temblarHit){
            case 3:px=px-3;break;
            case 2:px=px+5;break;
            case 1:px=px-2;break;
        }
        temblarHit--;
    }
    public void suspenderHit(){
        suspenderHit--;
    }
   
}//end class
