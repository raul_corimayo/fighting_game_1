package Game1;

import java.awt.Image;

public class Sprite {
//maneja el recurso imagen, no donde se dibujara ni el tama�o final
	
	private Image image;
	private int t;
	private int n;
	private int tMax;//valor maximo que puede tomar t
	private int nMax;//valor maximo que puede tomar n
	private int ancho;
	private int alto;
	private boolean ciclico;
	//private int duracion;//duracion de cada frame
	
	public Sprite(Image image, int ancho, int alto) {
		super();
		this.image = image;
		this.ancho = ancho;
		this.alto = alto;
		calcularTMaxNMax();
	}
	
	public void resetAnimation(){
		t = 0;
	}
	
	public boolean nextFrame(){
	//devuelve true si hay siguiente, false si no hay siguiente
		boolean aux = true;
		if(t<tMax){
			t++;
		}else{
			aux = false;
			if(ciclico){
				t = 0;				
			}
		}
		return aux;
	}
	
	public boolean setEstado(int n){
		boolean aux = false;
		if(0<=n && n<=nMax){
			this.n = n;
			aux = true;
		}
		return aux;
	}	
	
	private void calcularTMaxNMax(){
		tMax = image.getWidth(null)/ancho;
		nMax = image.getHeight(null)/alto;
		if(0<tMax){
			tMax-=1;
			}
		if(0<nMax){
			nMax-=1;
		}
	}
	
	//sx0, sxf, sy0, syf: coordenadas (sobre la imagen)
	// del frame actual del sprite 
	public int sx0(){return t*ancho;}
	public int sy0(){return n*alto;}
	public int sxf(){return (t+1)*ancho;}	
	public int syf(){return (t+1)*alto;}
	
}
