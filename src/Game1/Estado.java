package Game1;

public class Estado{
//Atributos--------------------------------------------------------------------//
    public NameEstado nombre;//nombre del estado
    public int n;//numero de poses
    public int[] p;//orden de las poses
    public int[][] z;//x0,y0,x,y en el mapa de poses
    public int[][][] r;//rectangulos auxiliares (AREAS: ataque, vulnerable, etc.)
        //r[posic][s*][x0,y0,x,y]---> s* = [0]centro, [1]vulnerable, [2]ataque, [3]vulnerableAdd
        //NOTA: los rectangulos no incluyen los limites del mismo (x0<x<xf and yo<y<yf)
//Metodos---------------------------------------------------------------------//
    void init(int id){
        Normal();
    }
    private void load(NameEstado nombre, int n, int[] p, int[][] z, int[][][] r){
        this.nombre=nombre;
        this.n=n;
        this.p=p;
        this.z=z;
        this.r=r;
    }
//Servicios para SuperClase
    public int x0(int t){return z[p[t]][0];}
    public int y0(int t){return z[p[t]][1];}
    public int x (int t){return z[p[t]][2];}
    public int y (int t){return z[p[t]][3];}
    public int cx (int t){return r[p[t]][0][0];}
    public int cy (int t){return r[p[t]][0][1];}
    public int d1x(int t){return r[p[t]][0][0]-z[p[t]][0];}
    public int d2x(int t){return z[p[t]][2]-r[p[t]][0][0];}
    public int d1y(int t){return r[p[t]][0][1]-z[p[t]][1];}
    public int d2y(int t){return z[p[t]][3]-r[p[t]][0][1];}
    //q = numero (id) de rctangulo = { centro=0 , vuln1=1 , vuln2=2 , ataq=3 }
    public int dqx0(int t,int q){return r[p[t]][q][0]-r[p[t]][0][0];}
    public int dqxf(int t,int q){return r[p[t]][q][2]-r[p[t]][0][0];}
    public int dqy0(int t,int q){return r[p[t]][q][1]-r[p[t]][0][1];}
    public int dqyf(int t,int q){return r[p[t]][q][3]-r[p[t]][0][1];}
//Gestion de Graficos--------------------------------------------------//
    public void Normal() {
        NameEstado nombreX = NameEstado.Normal;
        int [] pX = {0,0,0,0,0,0,0,0,0,0,1,1,1,1,2,2,2,2,1,1}; // n(pX)=20; max(pX)=2;
        int nX = pX.length;
        int [][] zX = {{0, 0, 69, 119}, {71, 0, 140, 119}, {142, 0, 211, 119}};//{x0,y0,x,y}
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{33,42,0,0},{5,6,66,119},{0,0,0,0},{0,0,0,0}},//0
            {{104,42,0,0},{76,6,137,119},{0,0,0,0},{0,0,0,0}},//1
            {{175,42,0,0},{147,6,208,119},{0,0,0,0},{0,0,0,0}}//2
        };
        load(nombreX, nX, pX, zX, rX);
    }public void NormalAvanza() {
        NameEstado nombreX = NameEstado.NormalAvanza;
        int [] pX = {0,0};
        int nX = pX.length;
        int [][] zX = {{213,0,284,119}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{246,42,0,0},{218,6,281,119},{0,0,0,0},{0,0,0,0}}//0
        };
        load(nombreX, nX, pX, zX, rX);
    }public void Avanza() {
        NameEstado nombreX = NameEstado.Avanza;
        int [] pX = {0,0,1,1,2,2,3,3};
        int nX = pX.length;
        int [][] zX = {{286,0,358,119},{360,0,433,119},{435,0,506,119},{508,0,579,119}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{319,42,0,0},{291,6,355,119},{0,0,0,0},{0,0,0,0}},//0
            {{393,42,0,0},{365,6,433,119},{0,0,0,0},{0,0,0,0}},//1
            {{468,42,0,0},{440,6,503,119},{0,0,0,0},{0,0,0,0}},//2
            {{541,42,0,0},{513,6,576,119},{0,0,0,0},{0,0,0,0}} //3
        };
        load(nombreX, nX, pX, zX, rX);
    }public void NormalRetrocede() {
        NameEstado nombreX = NameEstado.NormalRetrocede;
        int [] pX = {0,0};
        int nX = pX.length;
        int [][] zX = {{182,121,245,240}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{215,163,0,0},{187,127,242,240},{0,0,0,0},{0,0,0,0}},//0
        };
        load(nombreX, nX, pX, zX, rX);
    }public void Retrocede() {
        NameEstado nombreX = NameEstado.Retrocede;
        int [] pX = {0,0,1,1,2,2,3,3};
        int nX = pX.length;
        int [][] zX = {{247,121,310,240},{312,121,375,240},{377,121,446,240},{448,121,514,240}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{280,163,0,0},{252,127,307,240},{0,0,0,0},{0,0,0,0}},//0
            {{345,163,0,0},{317,127,372,240},{0,0,0,0},{0,0,0,0}},//1
            {{410,163,0,0},{382,127,443,240},{0,0,0,0},{0,0,0,0}},//2
            {{481,163,0,0},{453,127,511,240},{0,0,0,0},{0,0,0,0}} //3
        };
        load(nombreX, nX, pX, zX, rX);
    }public void SaltoAd() {
        NameEstado nombreX = NameEstado.SaltoAd;
        int [] pX = {0,0,0,0,1,2,3,4,1,5,6,6};
        int nX = pX.length;
        int [][] zX = {{253,364,314,481},
        {606,408,649,470},{561,363,623,406},{561,408,604,470},{625,363,687,406},
        {367,363,422,490},{316,363,365,481}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{284,404,0,0},{263,369,309,482},{0,0,0,0},{0,0,0,0}},//0
            {{626,430,0,0},{606,409,642,452},{0,0,0,0},{0,0,0,0}},//1
            {{601,383,0,0},{579,363,622,406},{0,0,0,0},{0,0,0,0}},//2
            {{585,448,0,0},{566,427,602,469},{0,0,0,0},{0,0,0,0}},//3
            {{647,386,0,0},{627,367,669,404},{0,0,0,0},{0,0,0,0}},//4
            {{392,413,0,0},{373,381,417,491},{0,0,0,0},{0,0,0,0}},//5
            {{340,404,0,0},{321,370,361,482},{0,0,0,0},{0,0,0,0}} //6
        };
        load(nombreX, nX, pX, zX, rX);
    }public void SaltoAt() {
        NameEstado nombreX = NameEstado.SaltoAt;
        int [] pX = {0,0,0,0,1,2,3,4,1,5,6,6};
        int nX = pX.length;
        int [][] zX = {{367,363,422,490},
        {606,408,649,470},{625,363,687,406},{561,408,604,470},{561,363,623,406},
        {253,364,314,481},{316,363,365,481}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{392,413,0,0},{373,381,417,491},{0,0,0,0},{0,0,0,0}},//0
            {{626,430,0,0},{606,409,642,452},{0,0,0,0},{0,0,0,0}},//1
            {{647,386,0,0},{627,367,669,404},{0,0,0,0},{0,0,0,0}},//2
            {{585,448,0,0},{566,427,602,469},{0,0,0,0},{0,0,0,0}},//3
            {{601,383,0,0},{579,363,622,406},{0,0,0,0},{0,0,0,0}},//4
            {{284,404,0,0},{263,369,309,482},{0,0,0,0},{0,0,0,0}},//5
            {{340,404,0,0},{321,370,361,482},{0,0,0,0},{0,0,0,0}} //6
        };
        load(nombreX, nX, pX, zX, rX);
    }public void SaltoN() {
        NameEstado nombreX = NameEstado.SaltoN;
        int [] pX = {0,0,0,0,1,2,2,2,2,1,0,0};
        int nX = pX.length;
        int [][] zX = {{316,363,365,481},{424,363,478,469},{480,363,559,448}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{340,404},{321,370,361,482},{0,0,0,0},{0,0,0,0}},//0
            {{447,401,0,0},{428,369,473,470},{0,0,0,0},{0,0,0,0}},//1
            {{527,400,0,0},{494,371,553,449},{0,0,0,0},{0,0,0,0}},//2
        };
        load(nombreX, nX, pX, zX, rX);
    }public void GolpeAireGD() {
        NameEstado nombreX = NameEstado.GolpeAireGD;
        int [] pX = {0,1,1,1,1,1,1,1,1,0};
        int nX = pX.length;
        int [][] zX = {{0,483,69,555},{71,483,153,555}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{39,514,0,0},{16,491,61,552},{0,0,0,0},{0,0,0,0}},//0
            {{108,514,0,0},{86,491,131,552},{134,520,155,540},{135,521,154,539}}//1
        };
        load(nombreX, nX, pX, zX, rX);
    }public void GolpeAireGM() {
        NameEstado nombreX = NameEstado.GolpeAireGM;
        int [] pX = {0,0,1,2,3,3,3,3,3};
        int nX = pX.length;
        int [][] zX = {{0,557,66,644},{68,567,145,644},{147,573,241,644},{243,572,322,644}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{38,603,0,0},{17,580,60,635},{0,0,0,0},{0,0,0,0}},//0
            {{106,603,0,0},{85,580,128,635},{126,566,146,596},{126,566,146,596}},//1
            {{185,603,0,0},{164,580,207,635},{213,582,242,601},{213,582,242,601}},//2
            {{281,603,0,0},{260,580,303,635},{301,588,322,625},{301,588,322,625}}//3
        };
        load(nombreX, nX, pX, zX, rX);
    }public void GolpeAireGF() {
        NameEstado nombreX = NameEstado.GolpeAireGF;
        int [] pX = {0,0,1,2,2,1,1,1,1,1};
        int nX = pX.length;
        int [][] zX = {{155,487,232,570},{234,492,302,570},{304,492,397,570}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{191,523,0,0},{169,493,220,565},{0,0,0,0},{0,0,0,0}},//0
            {{270,523,0,0},{248,494,299,565},{0,0,0,0},{0,0,0,0}},//1
            {{340,523,0,0},{318,494,369,565},{379,538,403,562},{378,537,404,563}}//2
        };
        load(nombreX, nX, pX, zX, rX);
    }public void GolpeNGD() {
        NameEstado nombreX = NameEstado.GolpeNGD;
        int [] pX = {0,1,0};
        int nX = pX.length;
        int [][] zX = {{0,121,69,240},{71,121,181,240}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{33,163,0,0},{5,127,67,240},{0,0,0,0},{0,0,0,0}},//0
            {{104,163,0,0},{76,127,140,240},{141,136,181,150},{170,137,181,150}},//1
        };
        load(nombreX, nX, pX, zX, rX);
    }public void GolpeNGM() {
        NameEstado nombreX = NameEstado.GolpeNGM;
        int [] pX = {0,1,2,2,3,3,3,1,0};
        int nX = pX.length;
        int [][] zX = {{0,242,72,361},{74,242,146,361},{148,242,270,361},{272,242,365,361}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{33,284,0,0},{5,248,70,362},{0,0,0,0},{0,0,0,0}}, //0
            {{107,284,0,0},{93,248,143,362},{0,0,0,0},{0,0,0,0}},//1
            {{181,284,0,0},{161,250,217,362},{217,253,270,273},{233,253,269,270}},//2
            {{305,284,0,0},{286,249,341,361},{341,254,366,273},{0,0,0,0}} //3
        };
        load(nombreX, nX, pX, zX, rX);
    }public void GolpeNGF() {
        NameEstado nombreX = NameEstado.GolpeNGF;
        int [] pX = {0,0,1,2,2,2,2,2,1,1,1,0};
        int nX = pX.length;
        int [][] zX = {{0,363,73,481},{75,363,145,481},{147,363,251,481}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{37,404,0,0},{3,369,63,482},{0,0,0,0},{0,0,0,0}}, //0
            {{107,404,0,0},{80,370,146,482},{0,0,0,0},{0,0,0,0}},//1
            {{170,404,0,0},{163,385,233,482},{222,388,252,426},{222,388,252,426}},//2
        };
        load(nombreX, nX, pX, zX, rX);
    }public void GolpeadoN1() {
        NameEstado nombreX = NameEstado.GolpeadoN1;
        int [] pX = {2,2,2,1,1,0};
        int nX = pX.length;
        int [][] zX = {{367,242,437,361},{439,242,510,361},{512,242,586,361}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{401,284,0,0},{372,248,434,361},{0,0,0,0},{0,0,0,0}},//0
            {{474,284,0,0},{444,248,507,361},{0,0,0,0},{0,0,0,0}},//1
            {{550,284,0,0},{517,248,583,361},{0,0,0,0},{0,0,0,0}},//2
        };
        load(nombreX, nX, pX, zX, rX);
    }public void GolpeadoAire1() {
        NameEstado nombreX = NameEstado.GolpeadoAire1;
        int [] pX = {0,0,0,0,0,0,0,0,0,0,0,0};
        int nX = pX.length;
        int [][] zX = {{0,646,56,775},{58,646,109,773},{111,649,181,765},{183,649,257,766}};
        int [][][] rX ={
            //  centro  ,   vuln #1  ,   vuln #2  ,    ataque
            {{13,691,0,0},{4,649,41,768},{0,0,0,0},{0,0,0,0}},//0
            {{80,691,0,0},{64,650,101,765},{0,0,0,0},{0,0,0,0}},//1
            {{148,690,0,0},{117,652,171,759},{0,0,0,0},{0,0,0,0}},//2
            {{229,690,0,0},{191,652,251,760},{0,0,0,0},{0,0,0,0}} //3
        };
        load(nombreX, nX, pX, zX, rX);
    }//end method

}//end class
