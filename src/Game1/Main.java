
package Game1;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

public class Main {

    public static void main(String[] args) throws Exception{
        JFrame jf = new JFrame("CRAZY-GAME (1.0) Raul Corimayo");
        jf.addWindowListener(new WindowAdapter() {
            @Override
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
        jf.setResizable(false);
        Ventana ventana = new Ventana();
        jf.getContentPane().add(ventana);        
        jf.pack();
        jf.setLocationRelativeTo(null);
        jf.setVisible(true);
        ventana.cicloPrincipalJuego();

    }

}
