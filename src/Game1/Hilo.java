package Game1;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;


public class Hilo extends Thread{
    String url;
    boolean localeSound;

    public Hilo(String name, String url, boolean localeSound){
        super(name);
        this.url = url;
        this.localeSound = localeSound;
    }

    @Override
    public void run(){
        InputStream path;  
        path = getClass().getResourceAsStream(url);
        try{ 

            AudioInputStream audioStream;

            if(localeSound){
                InputStream bufferedIn = new BufferedInputStream(path);
                audioStream =  AudioSystem.getAudioInputStream(bufferedIn);
            }else{
                File file = new File(url);
                audioStream =  AudioSystem.getAudioInputStream(file);
            }      
            Clip sound = AudioSystem.getClip();
            sound.open(audioStream);            
            sound.start();
            sleep(100);
            while (sound.isRunning()){
                sleep(500);
            }
            sound.close();
        }catch(Exception fallo){
            System.out.println("FALLO"+fallo);
        }
    }
}

